'use script'
window.addEventListener('load', () => {
 var formulario = document.getElementById("formulario");
 var box_dashed = document.querySelector(".dashed");
 box_dashed.style.display = "none";
  
  
   formulario.addEventListener('submit', () => {
        console.log("Evento submit capturado");

        box_dashed.style.display = "block";

    var nombre = document.querySelector("#nombre").value;
    var apellido = document.querySelector("#apellidos").value;
    var edad = parseInt(document.querySelector("#edad").value);

   //validar los datos

    if(nombre == null || nombre.trim().length == 0 || !isNaN(nombre)){
        alert("Nombre no valido");
        return false; //el return false corta el script (detiene la ejecucion)
    }

    if(apellido == null || apellido.trim().length == 0 || !isNaN(apellido)){
        alert("apellido no valido");
        return false;
    }

    if(edad == null || edad <= 0 || isNaN(edad)){
        alert("Edad no valida");
        return false;
    }

    console.log(nombre, apellido, edad);
   
    
    //AGREGAR ELEMENT
    var titulo = document.createElement("h3"); //agrega la etiqueta que escribamos
    var hr = document.createElement("hr");
    

    var texto = document.createTextNode("Informacion del usuario"); //el texto que sera ingresado en la etiqueta
    titulo.append(texto); //agregara el valor de la variable texto al final del existente
    

    box_dashed.prepend(titulo); //Agregalo a el bloque que deseemos
    box_dashed.append(hr);

    var datosUser = [nombre, apellido, edad];
    var nameDato = ["Nombre: ", "Apellido: ", "Edad: "];
  
    for(var indice in datosUser){
        var parrafo = document.createElement("p");
        parrafo.append(nameDato[indice] + datosUser[indice]);
        box_dashed.append(parrafo);
    }

   });

});