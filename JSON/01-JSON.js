/**
JSON  = Javascript object notation

son arrays asociativos dentro de Javascript
puedes meter variables dentro de variables 
 */

 window.addEventListener("load", () => {
    var serie = {
        titulo: 'Casa de papel',
        year: 2017,
        pais: 'España'
    }
   
    console.log(serie.titulo);
    console.log(serie.year);
   
    serie.titulo = "Scream";
    console.log(serie.titulo);
   
    var peliculas = [
        {titulo: 'deadpool', year: 2017, pais: 'EEUU'},
        serie
    ]
   
    var caja_peliculas = document.querySelector("#peliculas");
    
    for(var index in peliculas){
        var p = document.createElement("p");
        p.append(peliculas[index].titulo + " - " + peliculas[index].year);
       caja_peliculas.append(p);
    }
 })