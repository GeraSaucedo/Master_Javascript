'use strict'
$(document).ready(function(){
   
    if(window.location.href.indexOf('index') > -1){
   
        $('.slider').bxSlider({
            mode: 'fade',
            captions: true,
            slideWidth: 1200,
            responsive: true,
            pager: true
    
        });
    }   

    //post
    if(window.location.href.indexOf('index') > -1){
        var post = [
            {
                title: 'prueba de titulo 1',
                date: 'Fecha de publicacion: ' + moment().format("MMMM Do YYYY"), //Esto com momentjs (Cargar libreria)
                content: `Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                Blanditiis iusto recusandae, architecto optio minima quidem adipisci, 
                sint placeat asperiores ab nisi libero nulla, beatae cumque ea repudiandae 
                tenetur sunt quaerat!'`
            },
            {
                title: 'Prueba de titulo 2',
                date: 'Fecha de publicacion: ' + moment().format("MMMM Do YYYY"),
                content: `Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                Blanditiis iusto recusandae, architecto optio minima quidem adipisci, 
                sint placeat asperiores ab nisi libero nulla, beatae cumque ea repudiandae 
                tenetur sunt quaerat!'`
            },
            {
                title: 'Prueba de titulo 3',
                date: 'Fecha de publicacion: ' + moment().format("MMMM Do YYYY"),
                content: `Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                Blanditiis iusto recusandae, architecto optio minima quidem adipisci, 
                sint placeat asperiores ab nisi libero nulla, beatae cumque ea repudiandae 
                tenetur sunt quaerat!'`
            },
            {
                title: 'Prueba de titulo 4',
                date: 'Fecha de publicacion: ' + moment().format("MMMM Do YYYY"),
                content: `Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                Blanditiis iusto recusandae, architecto optio minima quidem adipisci, 
                sint placeat asperiores ab nisi libero nulla, beatae cumque ea repudiandae 
                tenetur sunt quaerat!'`
            },
            {
                title: 'Prueba de titulo 5',
                date: new Date(),
                content: `Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                Blanditiis iusto recusandae, architecto optio minima quidem adipisci, 
                sint placeat asperiores ab nisi libero nulla, beatae cumque ea repudiandae 
                tenetur sunt quaerat!'`
            }
    
        ];
    
        post.forEach((item,index) => {
            var post = `
            <article class="post">
                <h2>${item.title}</h2>
                    <span class="date">${item.date}</span>
                    <p>
                        ${item.content}
                    </p>
                <a href="#" class="button-more">Leer mas</a>
            </article>
            `;
    
            $('#posts').append(post);
    
        });
    }

    //SELECTOR DE TEMAS
    var theme = $('#theme')
    $('#blue').click( () => {
        theme.attr('href','css/blue.css');
    });
    
    $('#red').click( () => {
        theme.attr('href','css/red.css');
    });

    $('#yellow').click( () => {
        theme.attr('href','css/yellow.css');
    });
    

    //scroll arriba de la web
    $('.subir').click( function(e){
        e.preventDefault();

        $('html,body').animate({
            scrollTop: 0
        }, 500);

        return false;
    });

    //LOGIN FALSO
    $('#login').submit( () => {
        var form_name = $('#name').val();

        localStorage.setItem("form_name", form_name);
    
    });

    var form_name = localStorage.getItem("form_name");

    if(form_name != null && form_name != 'undefined'){

        var about_parrafo = $('#about p');
        about_parrafo.html("<strong>Bienvenido, " + form_name + "</strong>");
        about_parrafo.append("<a href='#' id='logout'> Cerrar Sesion </a>");

        $('#login').hide();

        $('#logout').click(() => {
            localStorage.clear();
            location.reload();
        });

    }

    
    
    if(window.location.href.indexOf('about') > -1){
        $('#acordeon').accordion();
    }

    if(window.location.href.indexOf('reloj') > -1){
        setInterval(() => {
            var reloj = moment().format('hh:mm:ss');
            $('#reloj').html(reloj);
        },1000);

        

    }

    if(window.location.href.indexOf('contact') > -1){

        $("form input[name='date']").datepicker({
            dateFormat: 'dd-mm-yy'
        });
        
        
        $.validate({
            lang: 'es',
            errorMessagePosition: 'top',
            scrollToTopOnError: true
          });
    }

  });