var fecha = new Date();
var year = fecha.getFullYear();
var mes = fecha.getMonth(); //Nota, el mes comienza desde el 0, enero = 0, diciembre = 11
var dia = fecha.getDay();
var hora = fecha.getHours();
var minutos = fecha.getMinutes();

console.log(fecha);

var textoHora = `
    El año es: ${year}
    El mes es: ${mes+1} 
    El dia es: ${dia}
    La hora es: ${hora + ":" + minutos}
    
`;

console.log(textoHora);

//FUNCIONES MATEMATICAS
console.log(Math.ceil(Math.random()*10));

