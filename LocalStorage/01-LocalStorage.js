/**
 * Local storage es una manera de almacenar informacion dentro del navegador y que persista hasta terminar la sesion
 * 
 */
//'use strict'

/**
 * en versiones mas antiguas de ciertos navegadores no esta disponible el loca storage.
 * Para comprobar si local storage esta disponible  escribimos:
 */
if(typeof(Storage) != 'undefined'){
    console.log("LocalStorage Disponible!");
}else {
    console.log("Incompatible con LocalStorage!");
}

window.addEventListener("load", () => {
    
    //Guardar datos en el local storage
    localStorage.setItem("titulo", "Curso de javascript"); //.setItem(key, value);

    //Recuperar elemento de local storage
    console.log(localStorage.getItem("titulo")); //getItem(key);


    var divpelis = document.querySelector("#peliculas");
    divpelis.innerHTML = localStorage.getItem("titulo");

    //Guardar objectos (Hay que convertirlos en JSON string)
    var usuario = {
        nombre: "Gerardo Saucedo",
        email: "jegsg@hotmail.es",
        phone: 1000000001    
    }

    localStorage.setItem("usuario", JSON.stringify(usuario));

    //Recuperar objeto JSON STRING
    var userjs = JSON.parse(localStorage.getItem("usuario"));
    console.log(userjs);

    document.querySelector("#datos").append(" " + userjs.nombre + " " + userjs.email);

    //funciones utiles
    //localStorage.clear(); //Eliminar todos los datos guardados en el loca storage
    //localStorage.removeItem("usuario"); //Eliminar el item indicado

});
