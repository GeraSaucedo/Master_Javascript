$(document).ready(() =>{
    console.log("JQuery listo!");
});

window.addEventListener('load', () =>{

    var elemento = $('.elemento');
    //EFECTOS DE JQUERY UI
    elemento.draggable(); //Draggable
    elemento.resizable(); //Resizable (Hay que agregar los links de los css y temas)
    //$('.listaselect').selectable();//SELECTABLE añade las clases ui-selecting ui-selected
    $('.listaselect').sortable({
        update: function(event, ui){
            console.log("Ha cambiado la lista");
        }
    }); //Ordenar los elementos

    //Droppable
    $('#movido').draggable();
    $('#area').droppable({
        drop: function(){
            console.log("Has soltado algo dentro de el area");
        }
    });

    //EFECTOS
    $('#mostrar').click(function(){
        //$('.cajaefecto').toggle("fade");
        //$('.cajaefecto').effect("explode");
        $('.cajaefecto').toggle("explode");
    });

    //TOOLTIPS
    $(document).tooltip();

    //Dialog
    $('#lanza').click(function(){
        $('#popup').dialog();
    });
  
    //Calendario / date picker
    $('#calendario').datepicker();

    //Tabs
    $('#pestanas').tabs();
//-------------------------------------------------------
    
  



});