/**
 * Cuando se usa jquery hay que asegurarse de que jquery esta listo para usarse
 */
window.addEventListener("load", () =>{

    $(document).ready(function() { //El simbolo de dolar es para llamar a jquery
        console.log("JQuery cargado");
    });
    
    //Tipos de selectores
    //SELECTOR ID (Para seleccionar por id se utiliza "#"  (#id))
    var color = $("#rojo").css("background","red")
                          .css("color","white");  
    
    var amarillo = $("#amarillo").css("background","yellow")
                                 .css("color", "brown");
    var verde = $("#verde").css("background","green")
                           .css("color","white");

    //SELECTORES DE CLASES
    //Los selectores se ubican por clase utilizando "."  (.ClassName)
    var mi_clase = $(".zebra");
    mi_clase.css("padding","5px");

    $(".sin_borde").click(function() { //checar why no se puede usar () =>
        console.log("click");
        $(this).addClass('zebra');
    });

    //SELECTORES DE ETIQUETA (Etiquetas html)
    var parrafos = $('p');  //Seleccionamos todos los parrafos
    parrafos.css('cursor','pointer');

    parrafos.click(function(){
        var that = $(this);
        if(that.hasClass('grande')){ //hasClass
            that.removeClass('grande');
        }else{
            that.addClass('grande');
        }

        //$(this).removeClass("zebra");
     });


     //SELECTORES DE ATRIBUTO
     $('[title="google"]').css("background","lime");
     $('[title="fb"').css("background","blue");
     $('[title="yt"]').css("background","red");


    //OTROS
    $('p, a').addClass('margen-superior'); //PODEMOS ELEGIR DIFERENTES ETIQUETAS 

    
    //FIND Y PARENT 
     var busqueda = $('#caja').find('.resaltado'); //se utiliza para buscar etiquetas que no sabemos donde estan 
     console.log(busqueda);

     var busqueda2 =  $('#caja').eq(0).parent().find('.resaltado'); //Sale a el bloque padre










});
