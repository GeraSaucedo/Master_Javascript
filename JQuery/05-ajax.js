$(document).ready(() =>{
    console.log("JQuery listo!")
});

window.addEventListener('load', () =>{

    //load
    //$('#datos').load("https://reqres.in/");  //carga el codigo html de lo indicado


    //GET
    $.get("https://reqres.in/api/users", { page: 3}, function(response) {
            response.data.forEach(element => {
                $("#datos").append('<p>'+ element.first_name+' ' + element.last_name + '</p>');
            });
    });

    
    //POST
    /* $.post("https://reqres.in/api/users", usuario, function(response){
        console.log(response);
    }); */

    $('#formulario').submit(function(e) {
        var usuario = {
            'nombre': $('[input="name"]').val(),
            'apellido': $('[input="web"]').val()
        };

        /*
        $.post($(this).attr('action'), usuario, function (response){
            console.log(response)
        }).done(function () {
            alert("Usuario añadido correctamente");
        });
        */


        $.ajax({
            type: 'POST',
            //dataType: 'json',
            //contentType: 'aplication/json',
            url: $(this).attr('action'),
            data: usuario,
            beforeSend: function(){
                console.log("Enviando usuario...");
            },
            succes: function(response){
                console.log(response);
            },
            error: function(){
                console.log("A ocurrido un error");
            },
            timeout: 2000
        });

        return false;
    });

});