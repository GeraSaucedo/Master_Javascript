$(document).ready(function(){
    console.log("Eventos cargados");    
});

window.addEventListener("load", () => {

    var caja = $('#caja');

    /*
    caja.mouseover(function(){ 
        $(this).css('background','blue');
    });

    caja.mouseout(function(){
        $(this).css('background','yellow');
    });
    */

    function cambiaRojo(){
        caja.css('background',"red");
    }

    function cambiaVerde(){
        caja.css('background',"lime");
    }
    //Hover
    caja.hover(cambiaRojo,cambiaVerde);


    //Click
    caja.click(function() {
        $(this).css('background','blue')
               .css('color','white');
    });
    //DobleClick
    caja.dblclick(function() {
        $(this).css('background','yellow')
               .css('color','black');
    });

    var nombre = $('#nombre');
    var datos = $('#datos');
    //Focus
    nombre.focus(function(){
        $(this).css('border','2px solid lime')
    });

    //blur
    nombre.blur(function(){
        $(this).css('border','2px solid transparent');
        datos.text($(this).val()).show(); //Obtener el valor y mostrar #datos
    });

    //mouse down (presionar tecla raton)
    datos.mousedown(function(){
        $(this).css('border-color','yellow');
        
    });

    //mouse up (soltar tecla raton)
    datos.mouseup(function(){
        $(this).css('border-color','red');
    });

    //mousemove
    $(document).mousemove(function(){ //Para ver donde esta el mouse 
       var sigueme = $('#sigueme');
       //$('body').css('cursor','none');
       sigueme.css('left',event.clientX);
       sigueme.css('top',event.clientY);
    });

   //EFECTOS
   $("#mostrar").click(function() {
        $(".caja-efectos").toggle("fade");
   });

});

