$(document).ready(function(){
    console.log("JQuery cargado!");
});

window.addEventListener("load",() => {

    
    //SHOW
    $('#mostrar').click(function(){
        //$('#caja').show('fast');
        //$('#caja').fadeIn('fast'); //desvanece el elemento
        $('#caja').fadeTo('slow', 0.8);
        $('#ocultar').show();
        $('#mostrar').hide();
    });

    //HIDE
    $('#ocultar').click(function(){
        //$('#caja').hide('fast'); 
        //$('#caja').fadeOut('fast'); //desvanece el elemento
        //$('#caja').fadeTo('slow', 0.2);
        $('#caja').slideUp('slow', () => {
            console.log("caja ocultada");  //SE PUEDEN USAR CALLBACKS EN LOS EFECTOS
        });
        $('#mostrar').show();
        $('#ocultar').hide();
        
    });

    $('#todo').click(function () {
        //$('#caja').toggle('fast');
        //$('#caja').fadeToggle('fast');
        //$('#caja').slideToggle('fast');
        $('#caja').slideDown('slow');

    });

    var caja = $('#caja');
    //HACER PROPIAS ANIMACIONES O ANIMACIONES CUSTOM 
    $('#animar').click(function () { 
        caja.animate({    marginLeft: '500px',  //recibe las propiedades como  json 
                                fontSize: '40px' ,
                                height: '110px'},
                                'slow')
            .animate({
                          borderRadius: '900px',
                          marginTop: '100px'
            },'slow')   
            .animate({
                          borderRadius: '0px',
                          marginLeft: '0px'
            },'slow')
            .animate({
                          borderRadius: '50px',
                          marginTop: '0px'
            },'slow');      
        
        
     });

    

});