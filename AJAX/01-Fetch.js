/**
 * Una peticion ajax es una peticion al backend o API 
 */

'use strict'

window.addEventListener("load", () => {

    var div_usuarios = document.getElementById("usuarios");
    var div_janet = document.getElementById("janet");

    var usuarios = [];

    //Fetch (ajax) y peticiones a servicios /api rest
    getUsuarios()   //regresa una promesa por lo que podemos encadenar con then
    .then(data => data.json()) 
    .then(users => {
        listadoUsuarios(users.data);
      
        return getInfo(); //Regresa otra promesa que podemos encadenar
    })
    .then(data => {
        console.log(data);
        return getJanet(); //Regresa una nueva promesa que podemos encadenar
    })
    .then(data => data.json())
    .then(user =>{
            mostrarJanet(user.data);
           
    })
    .catch(error => { //Metodo para capturar errores en las promesas 
        console.log('error');
    }); 
   

    //funciones

    function getUsuarios(){
        return fetch('https://reqres.in/api/users');
    }

    function getJanet(){
        return fetch('https://reqres.in/api/users/2');
    }

    function getInfo(){


        var profesor = {
            nombre: 'jerry',
            apellidos: 'Saucedo',
            url: 'https://www.google.com'
        };


        return new Promise((resolve, reject) => {
            var profesor_string;
            setTimeout(() => {
                profesor_string = JSON.stringify(profesor);
                if(typeof profesor_string != 'string') return reject('error j1');  
                return resolve(profesor_string); 
            }, 3000);
        });
    }

    function listadoUsuarios(usuarios){
        
        usuarios.map((user,i) => {
            let nombre = document.createElement("h2");
            nombre.innerHTML = i + user.first_name + " " + user.last_name
            div_usuarios.appendChild(nombre);

            document.querySelector(".loading").style.display = "none";
        });



    }

    function mostrarJanet(user){
            let nombre = document.createElement("h4");
            let avatar = document.createElement("img");

            nombre.innerHTML = user.first_name + " " + user.last_name
            avatar.src = user.avatar;
         
            div_janet.appendChild(nombre);
            div_janet.appendChild(avatar);

            document.querySelector("#janet .loading").style.display = "none";
    }
});